import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Usuario } from './modelo/usuario'
import { retry, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})



export class UsuariosService {
  base_path = 'http://localhost:8090/service/usuario';

  constructor(private http: HttpClient) { }

  getUsuario(id:any): Observable<Usuario> {
    console.log(this.base_path + '/' + id);
  return this.http
  .get<Usuario>(this.base_path + '/' + id)
  .pipe(
    retry(2),
    catchError(this.handleError)
  )
  }

  isUsuario(login:string, senha:string): Observable<boolean> {
    console.log(this.base_path + '/registrar?login=' + login +'&senha='+senha );
  return this.http
  .get<boolean>(this.base_path + '/registrar?login=' + login +'&senha='+senha)
  .pipe(
    retry(2),
    catchError(this.handleError)
  )
  }
  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };

}
