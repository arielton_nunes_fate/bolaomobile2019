export class Usuario{

private id:number;
private senha:string;
private nome:string;
private login:string;

constructor(id:number,senha:string,nome:string,login:string){
    this.id = id;
    this.login = login;
    this.nome = nome;
    this.senha = senha;
}

get Id():number{
    return this.id;
}
set Id(id:number){
    this.id = id;
}
get Senha():string{
    return this.senha;
}
set Senha(senha:string){
    this.senha = senha;
}

get Login():string{
    return this.login;
}
set Login(login:string){
    this.login = login;
}
get Nome():string{
    return this.nome;
}
set Nome(nome:string){
    this.nome = nome;
}
}