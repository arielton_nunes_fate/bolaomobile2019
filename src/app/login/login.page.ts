import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { UsuariosService } from '../usuarios.service';
import { Usuario } from '../modelo/Usuario';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})


export class LoginPage implements OnInit {
  data1:Usuario;
  constructor(public navctrl:NavController,public api: UsuariosService, public loadingController: LoadingController) { }
  resposta:Boolean;
  ngOnInit() {
    
}
isUsuario():Boolean {
   this.api.isUsuario('daSilva','54321').subscribe(response => {
    this.resposta = response;
  })
  console.log(this.resposta);
  return this.resposta;
}


  login(form){
    console.log("entrou");
    if (this.isUsuario()){
      this.navctrl.navigateRoot('home');
    }else{
      alert("Usuario não permitido");
    }
  }
}
